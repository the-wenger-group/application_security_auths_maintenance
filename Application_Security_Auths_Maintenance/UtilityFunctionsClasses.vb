﻿Option Explicit On
Imports System
Imports System.IO
Imports System.Text
Imports System.Data
Imports System.Xml
Imports System.Xml.Serialization
Imports System.Runtime.Serialization.Formatters
Imports IBM.Data.DB2.iSeries

' *********************************************************************************************************************************************************************
' This module contains utility functions and classes.
' *********************************************************************************************************************************************************************



' *********************************************************************************************************************************************************************
' Utility functions can be defined in the UtilityFunctions module as needed; otherwise, classes will generally be used to define such functions.
' *********************************************************************************************************************************************************************
Module UtilityFunctions



End Module

'**********************************************************************************************************************************************************************
' This class handles all of the JDE / DB2 related database interaction for the application.
'**********************************************************************************************************************************************************************
Public Class iSeriesDataHandler

    Private cnDB2 As iDB2Connection
    Private iAppSettings As New appConfig

    Private mDBError As Boolean
    Public ReadOnly Property DBError As Boolean
        Get
            Return mDBError
        End Get
    End Property

    Private mAuthsAvailable As DataSet
    Public ReadOnly Property AuthsAvailable As DataSet
        Get
            Return mAuthsAvailable
        End Get
    End Property

    Private mUserAuths As DataSet
    Public ReadOnly Property UserAuths As DataSet
        Get
            Return mUserAuths
        End Get
    End Property

    Public Sub New()
        ' When the class is instantiated, the connection is opened for the database.
        InitConnection()

    End Sub

    Public Sub InitConnection()
        ' This method is called ot initialize the database.  It instantiates the settings for the app to retrieve the connection string.

        Dim funcName As String = "iSeriesDataHandler.InitConnection"
        mDBError = False

        cnDB2 = New iDB2Connection
        Try
            cnDB2.ConnectionString = iAppSettings.iSeriesConnectionString
            cnDB2.Open()
        Catch ex As Exception
            MsgBox("Error:" & funcName & ":iSeries DB2 database connection error." & vbCrLf & "Error message: " & ex.Message & " : " & ex.ToString)
            mDBError = True
        End Try
    End Sub

    Public Function AuthExists(ByVal authCode As String) As Boolean
        ' This method returns true if the authCode passed in exists in the F56AUT02 table or false if it doesn't.

        Dim funcName As String = "iSeriesDataHandler.AuthExists"
        Dim returnValue As Boolean = False
        mDBError = False
        Dim cmdSQL As New iDB2Command
        Dim rs As iDB2DataReader = Nothing

        Try
            cmdSQL.CommandText = "select wfacod from " & iAppSettings.CurrentLibrary.Trim & ".f56aut02 "
            cmdSQL.CommandText &= "where upper(wfacod) = '" & authCode.Trim.ToUpper & "' "
            cmdSQL.CommandTimeout = 0
            cmdSQL.Connection = cnDB2
            rs = cmdSQL.ExecuteReader
        Catch ex As Exception
            MsgBox("Error:" & funcName & "DB2 error retrieving auth code. Error is:" & ex.Message & " : " & ex.ToString & vbCrLf & ":SQL statement was: " & cmdSQL.CommandText.ToString)
            mDBError = True
        End Try
        If mDBError = False Then
            If rs.HasRows Then
                returnValue = True
            Else
                returnValue = False
            End If
        End If

        Return returnValue
    End Function

    Public Function UserAuthExists(ByVal userId As String, ByVal authCode As String) As Boolean
        ' This method returns true if the user / auth code combination exists in the F56AUT01 file, and false if it doesn't.

        Dim funcName As String = "iSeriesDataHandler.AuthExists"
        Dim returnValue As Boolean = False ' Default value.
        mDBError = False
        Dim cmdSQL As New iDB2Command
        Dim rs As iDB2DataReader = Nothing

        Try
            cmdSQL.CommandText = "select wfacod from " & iAppSettings.CurrentLibrary.Trim & ".f56aut01 "
            cmdSQL.CommandText &= "where upper(wfunam) = '" & userId.Trim.ToUpper & "' and "
            cmdSQL.CommandText &= "upper(wfacod) = '" & authCode.Trim.ToUpper & "' "
            cmdSQL.CommandTimeout = 0
            cmdSQL.Connection = cnDB2
            rs = cmdSQL.ExecuteReader
        Catch ex As Exception
            MsgBox("Error:" & funcName & "DB2 error retrieving user auth code from F56AUT01. Error is: " & ex.Message & " : " & ex.ToString & vbCrLf & ":SQL statement was: " & cmdSQL.CommandText.ToString)
            mDBError = True
        End Try
        If mDBError = False Then
            If rs.HasRows Then
                returnValue = True
            Else
                returnValue = False
            End If
        End If

        Return returnValue
    End Function

    Public Function WriteAuthCode(ByVal authCode As String, ByVal authDesc As String) As Boolean
        ' This method creates the iSeries record for the auth code passed in.  The return value in this case doesn't mean anything.

        Dim funcName As String = "iSeriesDataHandler.WriteAuthCode"
        Dim returnValue As Boolean = False
        mDBError = False
        Dim cmdSQL As New iDB2Command

        Try
            cmdSQL.CommandText = "insert into " & iAppSettings.CurrentLibrary.Trim & ".f56aut02 "
            cmdSQL.CommandText &= "(wfacod, wfdesc) values ("
            cmdSQL.CommandText &= "'" & authCode.Trim & "', "
            cmdSQL.CommandText &= "'" & authDesc.Trim & "') "
            cmdSQL.CommandTimeout = 0
            cmdSQL.Connection = cnDB2
            cmdSQL.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("Error:" & funcName & "DB2 error inserting new auth record.  Error is: " & ex.Message & " : " & ex.ToString)
            mDBError = True
        End Try

        Return returnValue
    End Function

    Public Sub GetAvailableAuths(ByVal userID As String)
        ' This routine returns the auths available for the user, which consists of the authorizations the user does not already
        ' have, as a dataset in the AuthsAvailable property.

        Dim funcName As String = "iSeriesDataHandler.GetAvailableAuths"
        mDBError = False
        mAuthsAvailable = New DataSet
        Dim sqlString As String = ""
        Dim da As iDB2DataAdapter

        sqlString = "select a.wfacod, a.wfdesc from " & iAppSettings.CurrentLibrary.Trim & ".f56aut02 a "
        sqlString &= "left exception join " & iAppSettings.CurrentLibrary.Trim & ".f56aut01 b on "
        sqlString &= "upper(b.wfunam) = '" & userID.Trim.ToUpper & "' and "
        sqlString &= "upper(b.wfacod) = upper(a.wfacod) "
        sqlString &= "order by a.wfacod "
        Try
            da = New iDB2DataAdapter(sqlString, cnDB2)
            da.Fill(mAuthsAvailable, "F56AUT02")
        Catch ex As Exception
            MsgBox("Error:" & funcName & "DB2 error retrieving available auth codes.  Error is: " & ex.Message & " : " & ex.ToString)
            mDBError = True
        End Try

    End Sub

    Public Sub GetUserAuths(ByVal userID As String)
        ' This routine returns the current user auth data from the iSeries as a dataset in the UserAuths property.

        Dim funcName As String = "iSeriesDataHandler.GetUserAuths"
        mDBError = False
        mUserAuths = New DataSet
        Dim sqlString As String = ""
        Dim da As iDB2DataAdapter

        sqlString = "select wfunam, wfacod, wfcstr, wfcnum, wfcboo from " & iAppSettings.CurrentLibrary.Trim & ".f56aut01 "
        sqlString &= "where upper(wfunam) = '" & userID.Trim.ToUpper & "' "
        sqlString &= "order by wfunam, wfacod "
        Try
            da = New iDB2DataAdapter(sqlString, cnDB2)
            da.Fill(mUserAuths, "F56AUT01")
        Catch ex As Exception
            MsgBox("Error: " & funcName & "DB2 error retriving current user auths from F56AUT01. Error is: " & ex.Message & " : " & ex.ToString)
            mDBError = True
        End Try

    End Sub

    Public Sub WriteUserAuthRecord(ByVal userId As String, ByVal authCode As String)
        ' This routine creates a user auth record using the data passed in.

        Dim funcName As String = "iSeriesDataHandler.WriteUserAuthRecord"
        mDBError = False
        Dim cmdSQL As New iDB2Command

        Try
            cmdSQL.CommandText = "insert into " & iAppSettings.CurrentLibrary.Trim & ".f56aut01 "
            cmdSQL.CommandText &= "(wfunam, wfacod, wfcstr, wfcnum, wfcboo, wfadat, wfatim, wfausr) values ("
            cmdSQL.CommandText &= "'" & userId.Trim.ToUpper & "', "
            cmdSQL.CommandText &= "'" & authCode.Trim & "', "
            cmdSQL.CommandText &= "'', 0, 0, "
            cmdSQL.CommandText &= "'" & Now.ToString("MM/dd/yyyy") & "', "
            cmdSQL.CommandText &= "'" & Now.ToString("HH:mm:ss") & "', "
            cmdSQL.CommandText &= "'" & Environment.UserName & "') "
            cmdSQL.CommandTimeout = 0
            cmdSQL.Connection = cnDB2
            cmdSQL.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("Error:" & funcName & "DB2 error creating new user auth record in F56AUT01.  Error is: " & ex.Message & " : " & ex.ToString & vbCrLf & ":SQL command was: " & cmdSQL.CommandText.ToString)
            mDBError = True
        End Try

    End Sub

    Public Sub DeleteUserAuthRecord(ByVal userId As String, ByVal authCode As String)
        ' This routine deletes the user auth record for the values passed in.

        Dim funcName As String = "iSeriesDataHandler.DeleteUserAuthRecord"
        mDBError = False
        Dim cmdSQL As New iDB2Command

        Try
            cmdSQL.CommandText = "delete from " & iAppSettings.CurrentLibrary.Trim & ".f56aut01 "
            cmdSQL.CommandText &= "where "
            cmdSQL.CommandText &= "upper(wfunam) = '" & userId.Trim.ToUpper & "' and "
            cmdSQL.CommandText &= "upper(wfacod) = '" & authCode.Trim.ToUpper & "' "
            cmdSQL.CommandTimeout = 0
            cmdSQL.Connection = cnDB2
            cmdSQL.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("Error:" & funcName & "DB2 error delete using auth record in F56AUT01. Error is: " & ex.Message & " : " & ex.ToString & vbCrLf & ":SQL command was: " & cmdSQL.CommandText.ToString)
            mDBError = True
        End Try
    End Sub

    Public Sub DeleteAuthRecord(ByVal authCode As String)
        ' This routine deletes the F56AUT02 record for the auth code passed in.  It does not delete corresponding user records.

        Dim funcName As String = "iSeriesDataHandler.DeleteAuthRecord"
        mDBError = False
        Dim cmdSQL As New iDB2Command

        Try
            cmdSQL.CommandText = "delete from " & iAppSettings.CurrentLibrary.Trim & ".f56aut02 "
            cmdSQL.CommandText &= "where "
            cmdSQL.CommandText &= "upper(wfacod) = '" & authCode.Trim.ToUpper & "' "
            cmdSQL.CommandTimeout = 0
            cmdSQL.Connection = cnDB2
            cmdSQL.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("Error:" & funcName & "DB2 error deleting auth record in F56AUT02. Error is: " & ex.Message & " : " & ex.ToString & vbCrLf & ":SQL command was: " & cmdSQL.CommandText.ToString)
            mDBError = True
        End Try
    End Sub

End Class

' *********************************************************************************************************************************************************************
' This class handles the extraction of settings from the application configuration file.  The New method is automatically called
' which opens the config file and loads the class properties.
' *********************************************************************************************************************************************************************
Public Class appConfig

    Private mConfigFile As String = "appConfig.xml"
    Private ReadOnly Property ConfigFile As String
        Get
            Return mConfigFile
        End Get
    End Property

    Private mConfigError As Boolean
    Public ReadOnly Property ConfigError As Boolean
        Get
            Return mConfigError
        End Get
    End Property

    Private miSeriesConnectionString As String
    Public ReadOnly Property iSeriesConnectionString As String
        Get
            Return miSeriesConnectionString
        End Get
    End Property

    Private mCurrentLibrary As String
    Public ReadOnly Property CurrentLibrary As String
        Get
            Return mCurrentLibrary
        End Get
    End Property

    Public Sub New()
        ' If the settings file exists, then proceed with processing.  Otherwise, give an error which the app can check to terminate itself.
        mConfigError = False ' Default value.
        Dim funcName As String = "appConfig.New"

        If File.Exists(ConfigFile) Then
            Dim configXML As New DataSet
            configXML.ReadXml(ConfigFile)

            ' Set the settings.
            miSeriesConnectionString = configXML.Tables("settings").Rows(0).Item("iSeriesConnectionString").ToString.Trim
            mCurrentLibrary = configXML.Tables("settings").Rows(0).Item("currentLibrary").ToString.Trim
            
            ' Check the settings and edit anything needed.
            If miSeriesConnectionString = Nothing Or miSeriesConnectionString = "" Then
                Console.WriteLine("Error:" & funcName & ":Invalid iSeriesConnectionString setting in config settings file.")
                mConfigError = True
            End If
            If mCurrentLibrary = Nothing Or mCurrentLibrary = "" Then
                Console.WriteLine("Error:" & funcName & ":Invalid currentLibrary setting in config settings file.")
                mConfigError = True
            End If
             Else
            MsgBox("No configuration file exists!  Supply config file and rerun application.")
            mConfigError = True
        End If
    End Sub
End Class

' *********************************************************************************************************************************************************************
' This class contains all of the methods used to handle form operations for the application.
' *********************************************************************************************************************************************************************
Public Class formHandler

    Private iSeriesData As New iSeriesDataHandler
    Private mUserAuths As DataSet
    Private mAvailableAuths As DataSet

    Private mHandlerError As Boolean
    Public ReadOnly Property HandlerError As Boolean
        Get
            Return mHandlerError
        End Get
    End Property

    Private mHandlerErrorMessage As String
    Public ReadOnly Property HandlerErrorMessage As String
        Get
            Return mHandlerErrorMessage
        End Get
    End Property

    Public Sub New()
        ' Any initialization needed upon instantiation is coded here.
    End Sub

    Public Sub RefreshListViews()
        ' This routine is used to refresh the available authorizations and the current user authorizations list views.

        Dim funcName As String = "formHandler.RefreshListViews"
        mHandlerError = False
        mHandlerErrorMessage = ""
        mUserAuths = New DataSet
        mAvailableAuths = New DataSet
        Dim availAuthsRec(2) As String
        Dim availAuthsListItem As ListViewItem = Nothing
        Dim userAuthsRec(1) As String
        Dim userAuthsListItem As ListViewItem = Nothing

        With frmMain
            .lstAvailAuths.Items.Clear()
            .lstCurrUserAuths.Items.Clear()
            If .txtUserId.Text.Trim = "" Then ' No user id in the field; just exit.
                Exit Sub
            End If
            iSeriesData.GetAvailableAuths(.txtUserId.Text.Trim)
            mAvailableAuths = iSeriesData.AuthsAvailable
            iSeriesData.GetUserAuths(.txtUserId.Text.Trim)
            mUserAuths = iSeriesData.UserAuths
            For Each AuthRow As DataRow In mAvailableAuths.Tables("F56AUT02").Rows
                availAuthsRec.Initialize()
                availAuthsRec(0) = AuthRow.Item("WFACOD").ToString.Trim
                availAuthsRec(1) = AuthRow.Item("WFDESC").ToString.Trim
                availAuthsListItem = New ListViewItem(availAuthsRec)
                .lstAvailAuths.Items.Add(availAuthsListItem)
            Next
            For Each UserRow As DataRow In mUserAuths.Tables("F56AUT01").Rows
                userAuthsRec.Initialize()
                userAuthsRec(0) = UserRow.Item("WFACOD").ToString.Trim
                userAuthsListItem = New ListViewItem(userAuthsRec)
                .lstCurrUserAuths.Items.Add(userAuthsListItem)
            Next
        End With
    End Sub

    Public Sub DisableValuesFields()
        ' This routine clears the values fields on the form and disables them, along with the update values button.

        Dim funcName As String = "formHandler.DisableValuesFields"
        mHandlerError = False
        mHandlerErrorMessage = ""

        With frmMain
            .txtStringValue.Text = ""
            .txtNumericValue.Text = ""
            .txtBooleanValue.Text = ""
            .txtStringValue.Enabled = False
            .txtNumericValue.Enabled = False
            .txtBooleanValue.Enabled = False
            .btnUpdateValues.Enabled = False
        End With

    End Sub

    Public Function ParseOutListviewValue(ByVal inputString As String) As String
        ' This function takes the input value, which is the list view item returned by a list view click, and parses out the
        ' value of the item, which is contained in a pair of {} brackets.

        Dim funcName As String = "formHandler.ParseOutListviewValue"
        mHandlerError = False
        mHandlerErrorMessage = False
        Dim returnValue As String = ""

        returnValue = inputString.Substring(inputString.IndexOf("{") + 1, (inputString.IndexOf("}") - inputString.IndexOf("{")) - 1)
        If returnValue.Trim = "" Then
            mHandlerError = True
            mHandlerErrorMessage = "Empty value returned by parsing of listview line."
        End If

        Return returnValue
    End Function

End Class

' *********************************************************************************************************************************************************************
' This class contains all of the methods used to handle authorizations for the application.
' *********************************************************************************************************************************************************************
Public Class AuthHandler

    Private iSeriesData As New iSeriesDataHandler

    Private mAuthorized As Boolean
    Public ReadOnly Property Authorized As Boolean
        Get
            Return mAuthorized
        End Get
    End Property

    Sub New()
        ' Any initialization needed upon instantiation is performed here.
    End Sub

    Sub CheckAuth(ByVal authCode As String)
        ' This routine checks the authcode passed in for the user running the application, and if the user is authorized to that
        ' code (it exists in the F56AUT01 file) then the Authorized property is set to true.

        mAuthorized = False ' Default
        mAuthorized = iSeriesData.UserAuthExists(Environment.UserName.ToString, authCode)

    End Sub

End Class