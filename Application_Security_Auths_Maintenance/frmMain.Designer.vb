﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblVersion = New System.Windows.Forms.Label()
        Me.txtApplicationMessages = New System.Windows.Forms.TextBox()
        Me.txtUserId = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtAuthCode = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtAuthDesc = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnAddAuth = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtStringValue = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtNumericValue = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtBooleanValue = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnRetrieveUser = New System.Windows.Forms.Button()
        Me.lstAvailAuths = New System.Windows.Forms.ListView()
        Me.AuthCode = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Description = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lstCurrUserAuths = New System.Windows.Forms.ListView()
        Me.CurrAuthCode = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnUpdateValues = New System.Windows.Forms.Button()
        Me.txtInstructions = New System.Windows.Forms.TextBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(12, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(128, 104)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(146, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(351, 33)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Wenger's Feed Mill, Inc."
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(146, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(386, 31)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Application Auths Maintenance"
        '
        'lblVersion
        '
        Me.lblVersion.AutoSize = True
        Me.lblVersion.Location = New System.Drawing.Point(149, 76)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(97, 13)
        Me.lblVersion.TabIndex = 4
        Me.lblVersion.Text = "Application Version"
        '
        'txtApplicationMessages
        '
        Me.txtApplicationMessages.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtApplicationMessages.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApplicationMessages.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtApplicationMessages.Location = New System.Drawing.Point(152, 99)
        Me.txtApplicationMessages.Name = "txtApplicationMessages"
        Me.txtApplicationMessages.ReadOnly = True
        Me.txtApplicationMessages.Size = New System.Drawing.Size(344, 17)
        Me.txtApplicationMessages.TabIndex = 74
        Me.txtApplicationMessages.TabStop = False
        Me.txtApplicationMessages.Text = "Application starting..."
        '
        'txtUserId
        '
        Me.txtUserId.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUserId.Location = New System.Drawing.Point(85, 133)
        Me.txtUserId.Name = "txtUserId"
        Me.txtUserId.Size = New System.Drawing.Size(100, 24)
        Me.txtUserId.TabIndex = 76
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(29, 133)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(59, 18)
        Me.Label7.TabIndex = 75
        Me.Label7.Text = "User Id:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtAuthCode
        '
        Me.txtAuthCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAuthCode.Location = New System.Drawing.Point(85, 163)
        Me.txtAuthCode.Name = "txtAuthCode"
        Me.txtAuthCode.Size = New System.Drawing.Size(209, 24)
        Me.txtAuthCode.TabIndex = 78
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(8, 163)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(81, 18)
        Me.Label3.TabIndex = 77
        Me.Label3.Text = "Auth Code:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtAuthDesc
        '
        Me.txtAuthDesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAuthDesc.Location = New System.Drawing.Point(85, 193)
        Me.txtAuthDesc.Name = "txtAuthDesc"
        Me.txtAuthDesc.Size = New System.Drawing.Size(321, 24)
        Me.txtAuthDesc.TabIndex = 80
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(9, 193)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 18)
        Me.Label4.TabIndex = 79
        Me.Label4.Text = "Auth Desc:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnAddAuth
        '
        Me.btnAddAuth.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddAuth.Location = New System.Drawing.Point(300, 161)
        Me.btnAddAuth.Name = "btnAddAuth"
        Me.btnAddAuth.Size = New System.Drawing.Size(106, 28)
        Me.btnAddAuth.TabIndex = 81
        Me.btnAddAuth.TabStop = False
        Me.btnAddAuth.Text = "Add Auth"
        Me.btnAddAuth.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(12, 229)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(163, 18)
        Me.Label5.TabIndex = 83
        Me.Label5.Text = "Available Authorizations"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(377, 229)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(191, 18)
        Me.Label6.TabIndex = 85
        Me.Label6.Text = "Current User Authorizations"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtStringValue
        '
        Me.txtStringValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStringValue.Location = New System.Drawing.Point(492, 133)
        Me.txtStringValue.Name = "txtStringValue"
        Me.txtStringValue.Size = New System.Drawing.Size(117, 24)
        Me.txtStringValue.TabIndex = 87
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(406, 133)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(90, 18)
        Me.Label8.TabIndex = 86
        Me.Label8.Text = "String Value:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtNumericValue
        '
        Me.txtNumericValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumericValue.Location = New System.Drawing.Point(509, 163)
        Me.txtNumericValue.Name = "txtNumericValue"
        Me.txtNumericValue.Size = New System.Drawing.Size(100, 24)
        Me.txtNumericValue.TabIndex = 89
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(405, 163)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(108, 18)
        Me.Label9.TabIndex = 88
        Me.Label9.Text = "Numeric Value:"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtBooleanValue
        '
        Me.txtBooleanValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBooleanValue.Location = New System.Drawing.Point(509, 193)
        Me.txtBooleanValue.Name = "txtBooleanValue"
        Me.txtBooleanValue.Size = New System.Drawing.Size(100, 24)
        Me.txtBooleanValue.TabIndex = 91
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(406, 193)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(107, 18)
        Me.Label10.TabIndex = 90
        Me.Label10.Text = "Boolean Value:"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnRetrieveUser
        '
        Me.btnRetrieveUser.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRetrieveUser.Location = New System.Drawing.Point(191, 131)
        Me.btnRetrieveUser.Name = "btnRetrieveUser"
        Me.btnRetrieveUser.Size = New System.Drawing.Size(106, 28)
        Me.btnRetrieveUser.TabIndex = 77
        Me.btnRetrieveUser.Text = "Retrieve User"
        Me.btnRetrieveUser.UseVisualStyleBackColor = True
        '
        'lstAvailAuths
        '
        Me.lstAvailAuths.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.AuthCode, Me.Description})
        Me.lstAvailAuths.FullRowSelect = True
        Me.lstAvailAuths.Location = New System.Drawing.Point(12, 250)
        Me.lstAvailAuths.Name = "lstAvailAuths"
        Me.lstAvailAuths.Size = New System.Drawing.Size(362, 432)
        Me.lstAvailAuths.TabIndex = 93
        Me.lstAvailAuths.TabStop = False
        Me.lstAvailAuths.UseCompatibleStateImageBehavior = False
        Me.lstAvailAuths.View = System.Windows.Forms.View.Details
        '
        'AuthCode
        '
        Me.AuthCode.Text = "Auth Code"
        Me.AuthCode.Width = 138
        '
        'Description
        '
        Me.Description.Text = "Description"
        Me.Description.Width = 220
        '
        'lstCurrUserAuths
        '
        Me.lstCurrUserAuths.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.CurrAuthCode})
        Me.lstCurrUserAuths.FullRowSelect = True
        Me.lstCurrUserAuths.Location = New System.Drawing.Point(380, 250)
        Me.lstCurrUserAuths.Name = "lstCurrUserAuths"
        Me.lstCurrUserAuths.Size = New System.Drawing.Size(229, 432)
        Me.lstCurrUserAuths.TabIndex = 94
        Me.lstCurrUserAuths.TabStop = False
        Me.lstCurrUserAuths.UseCompatibleStateImageBehavior = False
        Me.lstCurrUserAuths.View = System.Windows.Forms.View.Details
        '
        'CurrAuthCode
        '
        Me.CurrAuthCode.Text = "Auth Code"
        Me.CurrAuthCode.Width = 225
        '
        'btnUpdateValues
        '
        Me.btnUpdateValues.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUpdateValues.Location = New System.Drawing.Point(503, 99)
        Me.btnUpdateValues.Name = "btnUpdateValues"
        Me.btnUpdateValues.Size = New System.Drawing.Size(106, 28)
        Me.btnUpdateValues.TabIndex = 95
        Me.btnUpdateValues.TabStop = False
        Me.btnUpdateValues.Text = "Update Values"
        Me.btnUpdateValues.UseVisualStyleBackColor = True
        '
        'txtInstructions
        '
        Me.txtInstructions.Location = New System.Drawing.Point(11, 688)
        Me.txtInstructions.Multiline = True
        Me.txtInstructions.Name = "txtInstructions"
        Me.txtInstructions.ReadOnly = True
        Me.txtInstructions.Size = New System.Drawing.Size(598, 136)
        Me.txtInstructions.TabIndex = 96
        Me.txtInstructions.Text = "Instructions."
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(621, 836)
        Me.Controls.Add(Me.txtInstructions)
        Me.Controls.Add(Me.btnUpdateValues)
        Me.Controls.Add(Me.lstCurrUserAuths)
        Me.Controls.Add(Me.lstAvailAuths)
        Me.Controls.Add(Me.btnRetrieveUser)
        Me.Controls.Add(Me.txtBooleanValue)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtNumericValue)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtStringValue)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.btnAddAuth)
        Me.Controls.Add(Me.txtAuthDesc)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtAuthCode)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtUserId)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtApplicationMessages)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Name = "frmMain"
        Me.Text = "frmMain - Application Authorizations Maintenance"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblVersion As System.Windows.Forms.Label
    Friend WithEvents txtApplicationMessages As System.Windows.Forms.TextBox
    Friend WithEvents txtUserId As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtAuthCode As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtAuthDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnAddAuth As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtStringValue As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtNumericValue As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtBooleanValue As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnRetrieveUser As System.Windows.Forms.Button
    Friend WithEvents lstAvailAuths As System.Windows.Forms.ListView
    Friend WithEvents AuthCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents Description As System.Windows.Forms.ColumnHeader
    Friend WithEvents lstCurrUserAuths As System.Windows.Forms.ListView
    Friend WithEvents CurrAuthCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnUpdateValues As System.Windows.Forms.Button
    Friend WithEvents txtInstructions As System.Windows.Forms.TextBox

End Class
