﻿Option Explicit On
Imports System
Imports System.IO
Imports System.Text
Imports System.Data
Imports System.Windows.Forms
Imports IBM.Data.DB2.iSeries

Public Class frmMain
    '**********************************************************************************************************************
    ' Program Notes and Comments
    '
    ' 5/3/2011 jdw - Initial coding.
    ' 12/6/2013 jdw - Checked into TFS.
    ' 07/06/16 jdw - Started enhancements to have application replace Active Directory security groups for apps.
    '
    ' This is the Application Authorizations Maintenance application; it is used to maintain security and access authorization codes for
    ' the various applications built in house that require such maintenance control, such as the pellet production tracking app.
    ' 
    ' NOTES
    '
    '**********************************************************************************************************************

    ' Global variables - each global variable defined must be commented as to its purpose and how it is used.

    Dim programVersion As String = "20110505a" ' Change this whenever a modification is made to the application.

    ' The various global variables are instantiated here for use in this module.
    Public configSettings As New appConfig
    Public iSeries As New iSeriesDataHandler
    Public formFunctions As New formHandler
    Public authFunctions As New AuthHandler

    Private Sub frmMain_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ' This routine handles initialization tasks when the form first loads.
        Dim errorFlag As Boolean = False

        authFunctions.CheckAuth("App_Auths_Maint")
        If authFunctions.Authorized = False Then
            MsgBox(Environment.UserName.ToString & " not authorized to run this application.")
            End ' Terminate the program.
        End If

        lblVersion.Text = programVersion
        lblVersion.Refresh()
        formFunctions.DisableValuesFields()

        ' The instructions at the bottom of the form are set up here in the text box provided for that purpose.
        Dim wrk1 As String = ""
        wrk1 = "INSTRUCTIONS" & vbCrLf
        wrk1 &= "To add an authorization code, fill in the two authorization fields and click the Add Auth button." & vbCrLf
        wrk1 &= "To display the authorizations for a user, enter the user name in the User Id field and click the Retrieve User button." & vbCrLf
        wrk1 &= "Left click on a line in the Available Authorizations list to add that authorization to the current user list.  Right click on a "
        wrk1 &= "line in the Available Authorizations list to delete that authorization code." & vbCrLf
        wrk1 &= "Left click on a line in the Current User Authorizations list to modify the values fields for that authorization.  Right click on a "
        wrk1 &= "line in the Current User Authorizations list to delete that user authorization code." & vbCrLf
        wrk1 &= "NOTE - values fields modification functionality not currently implemented."
        txtInstructions.Text = wrk1

    End Sub

    Private Sub btnRetrieveUser_Click(sender As System.Object, e As System.EventArgs) Handles btnRetrieveUser.Click
        ' This routine handles the retrieval of the user authorization data of the user specified in the user text box when this 
        ' button is clicked.  It does the following:
        ' - Calls the routine to refresh the listview boxes.
        ' - Clears the add auth fields on the form.
        ' - Clears the auth values fields on the form and disables them.

        If txtUserId.Text.Trim = "" Then ' Don't do anything; the user is blank.
            Exit Sub
        End If
        formFunctions.RefreshListViews()
        txtAuthCode.Text = ""
        txtAuthDesc.Text = ""
        formFunctions.DisableValuesFields()

    End Sub

    Private Sub btnAddAuth_Click(sender As System.Object, e As System.EventArgs) Handles btnAddAuth.Click
        ' This function adds the auth information specified in the two auth fields when the button is clicked.  It does the following:
        ' - Checks to make sure neither field is blank.
        ' - Checks to make sure the auth code is not already defined in the table.
        ' - Writes the auth code info to the table on the iSeries.
        ' - Clears the two auth fields.
        ' - Calls the routine to refresh the listview boxes.

        If txtAuthCode.Text.Trim = "" Or txtAuthDesc.Text.Trim = "" Then
            MsgBox("Both the auth code and description must be supplied to add a new auth code.")
            Exit Sub
        End If
        If iSeries.AuthExists(txtAuthCode.Text.Trim) = True Then
            MsgBox("This authorization code is already defined in the table.")
            Exit Sub
        End If
        iSeries.WriteAuthCode(txtAuthCode.Text.Trim, txtAuthDesc.Text.Trim)
        txtAuthCode.Text = ""
        txtAuthDesc.Text = ""
        formFunctions.RefreshListViews()

    End Sub

    Private Sub btnUpdateValues_Click(sender As System.Object, e As System.EventArgs) Handles btnUpdateValues.Click
        ' This function updates the current user auth record for which the values are displayed with whatever is in the values fields.  It does the following:
        ' - Updates the user auth record with the values in the three value fields.
        ' - Disables the three value fields and the update values button.
        ' - Enables the rest of the controls on the form.
        ' - Calls the routine to refresh the listview boxes.


        ' **************** Functionality not currently implemented.











    End Sub
  
    Private Sub lstAvailAuths_MouseClick(sender As System.Object, e As System.Windows.Forms.MouseEventArgs) Handles lstAvailAuths.MouseClick
        ' The MouseClick event needs to be used if you want to determine which button was clicked.
        ' This function handles the clicking of a line in the available auths listview box.  When an item is clicked, the following occurs:
        ' - We double check to make sure a user is specified in the user id text box.
        ' - The authorization clicked is written to the user authorization list table on the iSeries.  Note that it is assumed that the
        '   authorization does not already exist, since when the list of available authorizations is generated, current user auths are excluded.
        ' - The values fields in the authorization recorded added for the user are initialized to blanks / zero / false.
        ' - Calls the routine to refresh the listview boxes.

        Dim msgBoxReturn As Integer = 0
        If txtUserId.Text.Trim = "" Then ' No user; just exit.
            Exit Sub
        End If
        If e.Button = Windows.Forms.MouseButtons.Right Then ' Process as a deletion.
            msgBoxReturn = MsgBox("Click on OK to delete this authrorization code.", MsgBoxStyle.OkCancel, "Delete authorization code?")
            If msgBoxReturn = MsgBoxResult.Ok Then
                iSeries.DeleteAuthRecord(formFunctions.ParseOutListviewValue(lstAvailAuths.SelectedItems.Item(0).ToString))
                formFunctions.RefreshListViews()
            Else ' Cancel or something else was clicked; just exit.
                Exit Sub
            End If
        ElseIf e.Button = Windows.Forms.MouseButtons.Left Then ' Process as add auth to user list.
            iSeries.WriteUserAuthRecord(txtUserId.Text.Trim, formFunctions.ParseOutListviewValue(lstAvailAuths.SelectedItems.Item(0).ToString))
            formFunctions.RefreshListViews()
        Else ' Something else was clicked; ignore and exit.
            Exit Sub
        End If

    End Sub

    Private Sub lstCurrUserAuths_MouseClick(sender As System.Object, e As System.Windows.Forms.MouseEventArgs) Handles lstCurrUserAuths.MouseClick
        ' The MouseClick event needs to be used if you want to determine which button was clicked.
        ' This function handles the clicking of a line in the current user auths listview box.  When an item is clicked, the following occurs:
        ' - The routine determines whether it was a left click or a right click of the mouse.
        ' - If it was a left click:
        '   - A message box is displayed asking the user if they are sure they want to delete the auth from the user list.
        '   - If the message box returns true:
        '     - The user auth record is deleted from the iSeries table.
        '     - The routine to refresh the listview boxes is called.
        '  - If the message box returns false:
        '    - Nothing happens.
        ' - If it was a right click:
        '   - The three value fields are filled in with the current values of the auth record that was clicked.
        '   - The three value fields and the update values button are enabled.
        '   - The listview boxes, the auth code text boxes, and the user id box are disabled, along with their respective control buttons.
        '   - Control passes back to the program.  The user must then click on the update values button for further processing to continue;
        '     however, this routine is done.

        Dim msgBoxReturn As Integer = 0
        If txtUserId.Text.Trim = "" Then ' No user in the field; just exit.
            Exit Sub
        End If
        If e.Button = Windows.Forms.MouseButtons.Right Then ' Process as a deletion.
            msgBoxReturn = MsgBox("Click on OK to delete this authorization from this user.", MsgBoxStyle.OkCancel, "Delete user auth record?")
            If msgBoxReturn = MsgBoxResult.Ok Then
                iSeries.DeleteUserAuthRecord(txtUserId.Text.Trim, formFunctions.ParseOutListviewValue(lstCurrUserAuths.SelectedItems.Item(0).ToString()))
                formFunctions.RefreshListViews()
            Else ' Cancel or something else was clicked; just exit.    
                Exit Sub
            End If
        ElseIf e.Button = Windows.Forms.MouseButtons.Left Then ' Process value fields.

            '**************************** Functionality for modifying value fields is not currently implemented.


        Else ' Just exit; something else was clicked and we ignore it.
            Exit Sub
        End If

    End Sub

End Class
